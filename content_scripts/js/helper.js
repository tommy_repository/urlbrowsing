/**
 * Created by tomislavfabeta on 27/12/16.
 */

var URL;

function _GET(parameterName, done) {


    if (typeof done != 'function'){
        console.log('WTF', done);
    }
    chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
        URL = tabs[0].url;

        var params = {
            _get: {},
            pathParams: [],
            hashParams: {},
            host: '',
            _all: {},
            url: URL
        };
        var paramValue;

        params = GET_URL_PARAMS(URL);

        /**
         * If parameterName set, return value of this param
         */
        if (parameterName){
            for (var key in params._all){
                if (key == parameterName){
                    paramValue = params._all[key];
                }
            }

            return done(paramValue, params);
        }

        /**
         * If parameterName not set, return all
         */
        return done(params._all, params);
    });

}

/**
 *
 * @param url
 * @param setParams
 * @returns {string} url
 * @constructor
 */
function SET_URL_PARAMS(url, setParams){
    if (!setParams){
        setParams = {};
    }

    var params = {
        _get: {},
        pathParams: [],
        hashParams: {},
        host: '',
        _all: {},
        url: url
    };
    params = GET_URL_PARAMS(url, setParams);

    var url = params.host + '/';
    url += params.pathParams.join('/') + '?';

    for (var key in params._get){
        url+= key + '=' + params._get[key] + '&';
    }

    url += '#';
    for (var key in params.hashParams){
        url += key + '=' + params.hashParams[key] + '&';
    }
    return url;
}

/**
 * 
 * @param url
 * @param setParams
 * @returns {{_get: {}, pathParams: Array, hashParams: {}, host: string, _all: {}, url}}
 * @constructor
 */
function GET_URL_PARAMS(url, setParams){
    if (!setParams) setParams = {};

    var set_params_ = {};
    for (var key in setParams){
        set_params_[key.replace('_path_', '')] = setParams[key];
    }
    setParams = set_params_;

    var cleanRegex = new RegExp('&&', 'g');
    url = url.replace(cleanRegex, '&');

    var params = {
        _get: {},
        pathParams: [],
        hashParams: {},
        host: '',
        _all: {},
        url: url

    };
    params['___param_set___'] = [];


    var mainParts = url.split('?');
    var url = mainParts[0];
    var pathParams = url.split('/');
    var hostUrl = pathParams[0] + '//' +pathParams[1] + pathParams[2];
    var _getParams;

    if (mainParts.length > 1){
        _getParams = (mainParts[1]).split('&');
    }
    /**
     * HOST
     */
    params.host = hostUrl;
    /**
     * _GET params
     */
    for (var key in _getParams){
        var arr = _getParams[key].split('=');

        if(arr.length < 2) continue;
        var hashParams = (arr[1]).split('#');

        /**
         * If Hash param exists, save it
         */
        if (hashParams.length >1){
            arr[1] = hashParams[0];
            var hashParam = hashParams[1].split('=');
            params.hashParams[hashParam[0]] = decodeURIComponent(arr[2]);

            /**
             * Check if setting this param
             */
            if (setParams[hashParam[0]]){
                params.hashParams[hashParam[0]] = decodeURIComponent(setParams[hashParam[0]]);
                params['___param_set___'][hashParam[0]] = setParams[hashParam[0]];
            }
        }

        params._get[arr[0]] = decodeURIComponent(arr[1]);
        /**
         * Check if setting this param
         */
        if (setParams[arr[0]]){
            params._get[arr[0]] = decodeURIComponent(setParams[arr[0]]);
            params['___param_set___'][arr[0]] = setParams[arr[0]];
        }
    }
    /**
     * PATH params
     */
    for (var key in pathParams){
        if (key <= 2) continue;
        if (!(pathParams[key] && pathParams[key].length > 0)) continue;
        /**
         * Check if setting this param
         */
        if (setParams[pathParams[key]]){
            params.pathParams.push(decodeURIComponent(setParams[pathParams[key]]));
            params['___param_set___'][pathParams[key]] = setParams[pathParams[key]];
        }else{
            params.pathParams.push(decodeURIComponent(pathParams[key]));
        }
    }

    /**
     * Check if all params are set, if not add to _get as new
     */
    for (var key in setParams){
        if (!params['___param_set___'][key]){
            params._get[key] = setParams[key];
        }
    }
    /**
     * Push all params to _all, hash params and url path params
     */
    for (var key in params.hashParams){
        params._all[key] = params.hashParams;
    }
    for (var key in params.pathParams){
        params._all['_path_'+params.pathParams[key]] = params.pathParams[key];
    }
    for (var key in params._get){
        params._all[key] = params._get[key];
    }

    return params;
}

function _OPENTAB(options){
    if (!options){
        options = {};
    }
    options.url = options.url || config.new_tab_default_url;
    chrome.tabs.create(options);
}

function TO_ASCII(param, position){
    return param.charCodeAt(position || 0);
}

function FROM_ASCII(ascii){
    return String.fromCharCode(ascii)
}
function REPLACE_AT(param, index, character) {
    return param.substr(0, index) + character + param.substr(index+character.length);
}
function REVERSE_STRING(s) {
    return s.split('').reverse().join('');
}
function CHECK_IF_HAS_NUMBER(s){
    var reverse = REVERSE_STRING(s);
    var digitsMatch = reverse.match(/\d+/);

    if (!digitsMatch){
        return false;
    }

    /**
     * Find last number in string
     */
    var lastNumber = REVERSE_STRING(digitsMatch[0]);
    var index = (function(s){
        var index = 0;
        var matchNumberLength = (''+lastNumber).length;
        var stringWithoutMatchNumber = s.replace(lastNumber, '');
        index = stringWithoutMatchNumber.length;
        return index;
    })(s)

    var $return = {number:lastNumber,index:index};
    return (parseInt(lastNumber))? $return: false;
}
