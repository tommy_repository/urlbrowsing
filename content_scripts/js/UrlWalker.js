/**
 * Created by tomislavfabeta on 27/12/16.
 */

var UrlWalker = (function(){
    function UrlWalker(config){
        this.config = {
            paramsToWatch: {
                'q': 'String'
            },

            walkerButton: '.url_walker_button',
            paramsContainer: '#params-container'
        };

        this.urlParams = {};
        this.init();
    }

    UrlWalker.prototype.init = init;
    UrlWalker.prototype.getUrlParams = getUrlParams;
    UrlWalker.prototype.renderParamButtons = renderParamButtons;
    UrlWalker.prototype.goTo = goTo;
    UrlWalker.prototype.addEvents = addEvents;
    UrlWalker.prototype.addValue = addValue;
    UrlWalker.prototype.renderPopup = renderPopup;

    UrlWalker.prototype.goForward = goForward;
    UrlWalker.prototype.goBackwards = goBackwards;
    UrlWalker.prototype.getElement = getElement;


    /**
     * Public functions
     */
    function init(){
        var self = this;
        this.addEvents();
        this.renderPopup();
    }

    function renderPopup(){
        var self = this;

        this.getUrlParams(function(urlParams, obj){
            self.renderParamButtons(urlParams);
            // self.renderParamButtons(obj.references);
        });
    }

    function addEvents(){

        var self = this;

        $(this.config.walkerButton).each(function(){
            var action = $(this).data("action");
            var param = $(this).data("param");

            $(this).unbind( "click" );
            $(this).on('click', function(){

                if (action == 'forward'){
                    self.goForward(param);
                }else{
                    self.goBackwards(param);
                }
            });
        });
    }

    function getUrlParams(done){
        var self  =this;
        _GET(null, function(urlParams, obj){
            self.urlParams = urlParams;
            done(urlParams, obj);
        });
    }

    /**
     * @param howMuch
     * @param param
     * @param position
     * @returns {*}
     */

    function addValue(howMuch, param, position){

        if (position < 0){
            return false;
        }

        position = (position>=0)? position:param.length-1;

        if (isNaN(param)){

            var check_if_last_char_number = CHECK_IF_HAS_NUMBER(param);
            if (check_if_last_char_number){
                return param.substring(0,check_if_last_char_number.index) + '' +this.addValue(howMuch, check_if_last_char_number.number, position);
            }
            var ascii = TO_ASCII(param, position);

            ascii += howMuch;

            if (ascii > 126){
                return this.addValue(howMuch, param, position-1);
            }

            return REPLACE_AT(param, position, FROM_ASCII(ascii));
        }else{
            return parseInt(param)+howMuch;
        }
    }

    function renderParamButtons(urlParams){

        // $(this.config.paramsContainer).empty();
        $(this.config.paramsContainer).empty();

        for (var key in urlParams){
            var param = {
                param: key,
                paramValue: urlParams[key]
            };

            var element = this.getElement(param);

            $(this.config.paramsContainer).append(element);
        }

        this.addEvents();
    }

    function goTo(url){
        var self = this;

        chrome.tabs.getSelected(null, function(tab){
            var myNewUrl = url;

            chrome.tabs.update(tab.id, {url: myNewUrl}, function(tab){
                self.renderPopup();
            });
        });
    }

    function goBackwards(param) {

        var self = this;

        _GET(param, function(value, params){

            var url;
            var setUrlParams = {};

            if (value){
                setUrlParams[param] = self.addValue(-1, value);
            }

            url = SET_URL_PARAMS(params.url, setUrlParams);

            self.goTo(url);
        });
    }

    function goForward(param) {
        var self = this;

        _GET(param, function(value, params){
            var url;
            var setUrlParams = {};

            if (value){
                setUrlParams[param] = self.addValue(1, value);
            }

            url = SET_URL_PARAMS(params.url, setUrlParams);

            self.goTo(url);
        });
    }

    function getElement(params){
        var element = '<li class="list-group-item">{{paramValue}}{{text}}<div class="btn-group" role="group" aria-label="..."> <button type="button" class="btn btn-default url_walker_button"  data-action="backward" data-param="{{param}}"> << </button> <button type="button" class="btn btn-default url_walker_button" data-action="forward" data-param="{{param}}">>></button> </div></li>';
        if (!params.text){
            params.text = '';
        }

        for (var key in params){
            var reg = new RegExp('{{'+key+'}}', 'g');
            element = element.replace(reg, params[key]);
        }


        return element;
    }

    return UrlWalker;
})();
