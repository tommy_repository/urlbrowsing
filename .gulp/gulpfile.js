var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    minifyCss = require('gulp-minify-css'),
    watch = require('gulp-watch')

/**
 * Base js Path is content_scripts/js/
 */
var js = [
    'components/jquery/jquery.js',
    'components/tether/tether.min.js',
    'components/bootstrap/bootstrap.min.js',
    'config.js',
    'helper.js',
    'listeners.js',
    'UrlWalker.js',
    'main.js',
];
var background_js = [
    'background-main.js'
];

/**
 * Base css Path is content_scripts/css/
 */
var css = [
    'components/bootstrap/bootstrap.min.css',
    'main.css'
];

/**
 * Building configurations
 * @type {{baseJs: string, baseCss: string, jsBuild: string, cssBuild: string}}
 */

var build = {
    background_js:'background-min.js',
    js: 'min.js',
    css: 'min.css'
};

var path = {
    'baseScripts': '../content_scripts/',
    'baseJs': '../content_scripts/js/',
    'baseBackgroundJs': '../content_scripts/js/',
    'baseCss': '../content_scripts/css/',
    'jsBuild': '../content_scripts/build',
    'jsBackgroundBuild': '../content_scripts/build',
    'cssBuild': '../content_scripts/build',
};

for (var key in js){
    js[key] = path.baseJs + js[key];
}
for (var key in css){
    css[key] = path.baseCss + css[key];
}
for (var key in background_js){
    background_js[key] = path.baseBackgroundJs + background_js[key];
}

gulp.task('js', function () {
    console.log(background_js);
    console.log("Building... " + path.baseBackgroundJs);


    gulp.src(background_js)
        .pipe(uglify())
        .pipe(concat(build.background_js))
        .pipe(gulp.dest(path.jsBackgroundBuild));

    console.log(js);
    console.log("Building... " + path.jsBuild);

    return gulp.src(js)
        // .pipe(jshint())
        // .pipe(jshint.reporter('default'))
        .pipe(uglify())
        .pipe(concat(build.js))
        .pipe(gulp.dest(path.jsBuild));
});

gulp.task('css', function () {
    console.log(css);
    console.log("Building... " + path.cssBuild);


    // return gulp.src('js/*.js')
    return gulp.src(css)
        .pipe(minifyCss())
        .pipe(concat(build.css))
        .pipe(gulp.dest(path.cssBuild));
});

gulp.task('watch', function () {
    // Endless stream mode
    var files = path.baseScripts + '**/*.*';
    console.log("Watching ", files);
    return watch(files, { ignoreInitial: false })
        .pipe(gulp.dest('build'));
});

gulp.task('default', ['js', 'css'], function() {

});